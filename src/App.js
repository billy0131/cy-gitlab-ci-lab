import logo from "./logo.svg";
import "./App.css";


import TodoList from './ToDoList';

const todoList = [
  {
    id:1,
    value: 'Build With GitLab CI',
    status: true,
  },
  {
    id:2,
    value: 'Deploy to GitLab Page',
    status: false,
  },
  {
    id:3,
    value: 'Testing With GitLab CI',
    status: false,
  },
  {
    id:4,
    value: 'Release from tag',
    status: false,
  },
];

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p style={{marginTop: '2px'}}>
          version: {process.env.REACT_APP_VERSION}
        </p>
        <p style={{marginBottom: '5px'}}>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <TodoList todoList={todoList} />
      </header>
    </div>
  );
}

export default App;
